<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>home</a>
<a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>empresa</a>
<a href="{{ route('projetos') }}" @if(Tools::isActive('projetos*')) class="active" @endif>projetos</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>contato</a>

<div class="social">
    @foreach(['instagram', 'facebook'] as $s)
        @if($contato->{$s})
        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
        @endif
    @endforeach
</div>
