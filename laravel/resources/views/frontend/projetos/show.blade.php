@extends('frontend.common.template')

@section('content')

    <main class="projetos">
        <div class="center">
            <div class="projetos-show">
                <div class="informacoes">
                    <h1>{{ $projeto->titulo }}</h1>
                    <h2>{{ $projeto->subtitulo }}</h2>
                    <p>Ano: {{ $projeto->ano }}</p>

                    <div class="descricao">
                        {!! $projeto->descricao !!}
                    </div>

                    <div class="parceiros">
                        <h2>Parceiros</h2>
                        {!! $projeto->parceiros !!}
                    </div>
                </div>

                <div class="imagens">
                    @foreach($projeto->imagens as $imagem)
                    <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>

                <div class="navegacao">
                    @if($anterior)
                    <a href="{{ route('projetos.show', $anterior->slug) }}" class="anterior">
                        anterior
                    </a>
                    @endif
                    @if($proximo)
                    <a href="{{ route('projetos.show', $proximo->slug) }}" class="proximo">
                        próximo
                    </a>
                    @endif
                    <a href="{{ route('projetos') }}" class="menu">
                        menu
                    </a>
                    <a href="#" class="topo">
                        topo
                    </a>
                </div>
            </div>
        </div>
    </main>

@endsection
