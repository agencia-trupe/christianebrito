@extends('frontend.common.template')

@section('content')

    <main class="contato">
        <div class="center">
            <div class="wrapper">
                <h2>Contato</h2>
                <p>
                    {{ $contato->telefone }}<br>
                    {!! $contato->endereco !!}
                </p>
                <div class="social">
                    @foreach(['instagram', 'facebook'] as $s)
                        @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>

                <h2>Fale Conosco</h2>
                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="enviar">
                    <div class="response-wrapper">
                        <div id="form-contato-response">Preencha todos os campos corretamente.</div>
                    </div>
                </form>
            </div>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </main>

@endsection
