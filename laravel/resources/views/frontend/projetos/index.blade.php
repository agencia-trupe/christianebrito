@extends('frontend.common.template')

@section('content')

    <main class="projetos">
        <div class="center">
            <div class="projetos-thumbs">
                @foreach($projetos as $projeto)
                <a href="{{ route('projetos.show', $projeto->slug) }}">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                    <div class="overlay">
                        <span>{{ $projeto->titulo }}</span>
                        <span>{{ $projeto->subtitulo }}</span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </main>

@endsection
