import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.slide'
});

$(document).on('submit', '#form-contato', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            var error = 'Preencha todos os campos corretamente.';
            $response.fadeOut().text(error).fadeIn('slow');
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});

$('.topo').click(function(event) {
    event.preventDefault();

    $('html, body').animate({ scrollTop: 0 });
});
