@extends('frontend.common.template')

@section('content')

    <main class="empresa">
        <div class="center">
            <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" alt="">
            <div class="texto">
                <h2>{{ $empresa->titulo }}</h2>
                {!! $empresa->texto !!}
            </div>
        </div>
    </main>

@endsection
