<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index()
    {
        $projetos = Projeto::ordenados()->get();

        return view('frontend.projetos.index', compact('projetos'));
    }

    public function show(Projeto $projeto)
    {
        $projetosIds  = Projeto::ordenados()->lists('id')->toArray();
        $currentIdPos = array_search($projeto->id, $projetosIds);

        $anterior = array_key_exists($currentIdPos - 1, $projetosIds)
                    ? Projeto::find($projetosIds[$currentIdPos - 1])
                    : null;

        $proximo  = array_key_exists($currentIdPos + 1, $projetosIds)
                    ? Projeto::find($projetosIds[$currentIdPos + 1])
                    : null;

        return view('frontend.projetos.show', compact('projeto', 'anterior', 'proximo'));
    }
}
