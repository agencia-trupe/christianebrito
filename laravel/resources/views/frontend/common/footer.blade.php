    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ $config->nome_do_site }}
                <span>&middot;</span>
                Todos os direitos reservados
                <span>&middot;</span>
                <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe</a>
            </p>
        </div>
    </footer>
